#!/bin/bash
star=*;
way=`pwd`;
if [ $# -eq 0 ]
then 
crontab -l > cron.temp;
echo "Скрипт запущен из консоли!";
echo -e "\033[32m+------------------------+\033[0m";
echo -e "\033[32m|\033[0m\033[35m**********menu**********\033[32m|\033[0m";
echo -e "\033[32m+------------------------+\033[0m";
echo -e "\033[32m|\033[0m1-добавить процес       \033[32m|\033[0m";
echo -e "\033[32m|\033[0m2-редактировать процес  \033[32m|\033[0m";
echo -e "\033[32m|\033[0m3-удалить процес        \033[32m|\033[0m";
echo -e "\033[32m|\033[0m4-показать все          \033[32m|\033[0m";
echo -e "\033[32m+------------------------+\033[0m";
read menu;

case $menu in

1)echo "Введите имя процесса:";
read find;
echo "Как часто вы будете следить за процессом:";
echo "+---------------------------------------+";
echo "Min:";
read mm;
echo "Hours:";
read hh;
echo "Day:";
read day;
echo "Mount:";
read mount;
echo "WeeklyDay:";
read wd;
if [ "$mm" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$mm " >> cron.temp;
fi
if [ "$hh" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$hh " >> cron.temp;
fi
if [ "$day" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$day " >> cron.temp;
fi
if [ "$mount" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$mount " >> cron.temp;
fi
if [ "$wd" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$wd " >> cron.temp;
fi
echo -n -e "/home/dmitryeltex/Eltex/Linux/procview/procview.sh $find #procview\n" >> cron.temp;
crontab cron.temp;
rm ./cron.temp;;
2)echo "Введите имя процесса который вы хотите изменить:";
cat cron.temp | grep procview;
echo "-----------------------";
read movproc;
sed -i "/$movproc/d" cron.temp;
#----------------------------
echo "Min:";
read mm;
echo "Hours:";
read hh;
echo "Day:";
read day;
echo "Mount:";
read mount;
echo "WeeklyDay:";
read wd;
if [ "$mm" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$mm " >> cron.temp;
fi
if [ "$hh" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$hh " >> cron.temp;
fi
if [ "$day" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$day " >> cron.temp;
fi
if [ "$mount" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$mount " >> cron.temp;
fi
if [ "$wd" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$wd " >> cron.temp;
fi
echo -n -e "/home/dmitryeltex/Eltex/Linux/procview/procview.sh $movproc #procview\n" >> cron.temp;
crontab cron.temp;
rm ./cron.temp;;
#----------------------------
3)echo "Введите имя процесса который вы хотите удалить:";
cat cron.temp | grep procview;
echo "-----------------------";
read procdelete;
sed -i "/$procdelete/d" cron.temp;
crontab cron.temp;
rm ./cron.temp;;
4)cat cron.temp | grep procview;
echo "-----------------------";
rm ./cron.temp;;
*)echo "Неверный пункт меню";;
esac
else
	date=`date`;
	resulfind=`pidof $1`;
 	if [ -n "$resulfind" ]
 	then
 		echo "Accept-----------------------------------------" >> /home/dmitryeltex/Eltex/Linux/procview/error.log;
 		echo "$date : Процес $1 запущен и отслеживается" >> /home/dmitryeltex/Eltex/Linux/procview/error.log;
 		echo "-----------------------------------------------" >> /home/dmitryeltex/Eltex/Linux/procview/error.log;
 	else

 		echo "Error------------------------------------------" >> /home/dmitryeltex/Eltex/Linux/procview/error.log;
 		echo "$date" >> /home/dmitryeltex/Eltex/Linux/procview/error.log;
 		$1 2>>/home/dmitryeltex/Eltex/Linux/procview/error.log; #попытка запуска и запись в лог в случае ошибки
 		echo "-----------------------------------------------" >> /home/dmitryeltex/Eltex/Linux/procview/error.log;
 	fi
fi