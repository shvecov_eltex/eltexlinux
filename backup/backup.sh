#!/bin/bash

function interdata(){

echo "Как часто производить копирование:";
echo "+---------------------------------------+";
echo "Min:";
read mm;
echo "Hours:";
read hh;
echo "Day:";
read day;
echo "Mount:";
read mount;
echo "WeeklyDay:";
read wd;
if [ "$mm" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$mm " >> cron.temp;
fi
if [ "$hh" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$hh " >> cron.temp;
fi
if [ "$day" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$day " >> cron.temp;
fi
if [ "$mount" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$mount " >> cron.temp;
fi
if [ "$wd" = "$star" ]
	then echo -n "* " >> cron.temp;
else echo -n "$wd " >> cron.temp;
fi
}

star=*;
if [ $# -eq 0 ]
then
	crontab -l > cron.temp;
	echo -e "\033[32m+------------------------+\033[0m";
	echo -e "\033[32m|\033[0m\033[35m**********menu**********\033[32m|\033[0m";
	echo -e "\033[32m+------------------------+\033[0m";
	echo -e "\033[32m|\033[0m1-добавить backup       \033[32m|\033[0m";
	echo -e "\033[32m|\033[0m2-редактировать backup  \033[32m|\033[0m";
	echo -e "\033[32m|\033[0m3-удалить задачу        \033[32m|\033[0m";
	echo -e "\033[32m|\033[0m4-показать все          \033[32m|\033[0m";
	echo -e "\033[32m+------------------------+\033[0m";
	read menu;
	case $menu in
		1)read -p "Введите путь для backup'а: " source;
		read -p "Введите путь для хранения: " backupsource;
		interdata;
		echo -n -e "/home/dmitryeltex/Eltex/Linux/backup/backup.sh $source $backupsource #backup\n" >> cron.temp;
		crontab cron.temp;
		rm ./cron.temp;;
		2)echo "-----------------------";
		cat cron.temp | grep backup | sed "=";
		echo "-----------------------";
		read -p "Введите номер строки для изменения: " movebk;
		strmove=`cat cron.temp | grep backup | awk "FNR==$movebk{print}"`;
		if [ -n "$strmove" ]
		then
			grep -v "$strmove" cron.temp > out.txt;
			cp -f out.txt cron.temp;
			interdata;
			read -p "Введите новый источник для backup'а: " source;
			read -p "Введите новый путь для хранения: " backupsource;
			echo -n -e "/home/dmitryeltex/Eltex/Linux/backup/backup.sh $source $backupsource #backup\n" >> cron.temp;
			crontab cron.temp;
			rm ./out.txt;
		else
			echo "Нет такой строки";
		fi
		rm ./cron.temp;;
		3)echo "-----------------------";
		cat cron.temp | grep backup | sed "=";
		echo "-----------------------";
		read -p "Введите номер строки для удаления: " deletebk;
		str=`cat cron.temp | grep backup | awk "FNR==$deletebk{print}"`;
		if [ -n "$str" ]
		then
			grep -v "$str" cron.temp > out.txt;
			cp -f out.txt cron.temp;
			echo "Задача удалена";
			crontab cron.temp;
			rm ./out.txt;
		else
			echo "Нет такой строки";
		fi
		rm ./cron.temp;;
		4)echo "-----------------------";
		cat cron.temp | grep backup;
		echo "-----------------------";
		rm ./cron.temp;;
		*)echo "Неверный пункт меню";
		rm cron.temp;;
	esac
else
if [ -f "$1" ]
	then
	essence=1;
elif [ -d "$1" ]
	then 
	essence=2;
elif [ -b "$1" ]
	then essence=3;
else
	essence=0;
fi
case $essence in
	1)read -p "Архивировать копируемый файл? (y/n): " filetar;
if [ "$filetar" = "y" -o "$filetar" = "Y" ]
then
tar -czf $2/filebackup.tgz $1;
else
cp -f -p $1 $2;
fi;;
	2)read -p "Архивировать копируемый каталог? (y/n): " foldertar;
if [ "$foldertar" = "y" -o "$foldertar" = "Y" ]
then
tar -czf $2/folderbackup.tgz $1;
else
rsync -p -r --delete $1 $2;
fi;;
	3)dd if=$1 of=$2/blockdevicebackup.iso;;
	*)echo "некорректный ввод" > /home/dmitryeltex/Eltex/Linux/backup/backuperror.log;;
esac
fi