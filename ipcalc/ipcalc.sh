#!/bin/bash

read -p "Enter IP adress: " adress;
read -p "Enter mask: " mask;
adress_m=();
mask_m=();
mask_inv=();
network=();
host_min=0;
host_max=();
null="00000000";
i=0;
r=0;
while [ $i -lt ${#adress} ]
do
	temp='';
	while [ "${adress:$i:1}" != '.' -a $i -lt ${#adress} ]
	do
		temp=$temp${adress:$i:1};
		let "i+=1";
	done
	adress_m[r]=$temp;
	let "r+=1";
	let "i+=1";
done

i=0;
r=0;
while [ $i -lt ${#mask} ]
do
	temp='';
	while [ "${mask:$i:1}" != '.' -a $i -lt ${#mask} ]
	do
		temp=$temp${mask:$i:1};
		let "i+=1";
	done
	mask_m[r]=$temp;
	let "r+=1";
	let "i+=1";
done

for((i = 0; i < 4; i++))
do
temp_a=${adress_m[$i]};
temp_b=${mask_m[$i]};
network[$i]=`echo "$(($temp_a & $temp_b))"`;
done
for((i = 0; i < 4; i++))
do
	if [ ${mask_m[$i]} -eq 0 ]
	then
		invertmask=$invertmask$null;
	else
invertmask=$invertmask`echo "obase=2;${mask_m[$i]}" | bc`;
fi
done
numberone=`echo $invertmask | grep -o 1 | wc -l`;
let "numberone=(2**(32-$numberone))-2";
for((i = 0; i < 4; i++))
do
let "temp=255 - ${mask_m[$i]}";
mask_inv[$i]=$temp;
host_max[$i]=`echo "$((${adress_m[$i]} | ${mask_inv[$i]}))"`;
done
temp=${host_max[3]};
let "temp-=1"
host_max[3]=$temp;
echo "+---------------------------------------+";
echo -e "|Address:\t\t \033[34m${adress_m[0]}.${adress_m[1]}.${adress_m[2]}.${adress_m[3]}\033[0m    ";
echo -e "|Netmask:\t\t \033[34m${mask_m[0]}.${mask_m[1]}.${mask_m[2]}.${mask_m[3]}\033[0m      ";
echo "|=>                                     ";
echo -e "|Network:\t\t \033[34m${network[0]}.${network[1]}.${network[2]}.${network[3]}\033[0m      ";
let "host_min=${network[3]} + 1";
echo -e "|Hostmin:\t\t \033[34m${network[0]}.${network[1]}.${network[2]}.$host_min\033[0m      ";
echo -e "|Hostmax:\t\t \033[34m${host_max[0]}.${host_max[1]}.${host_max[2]}.${host_max[3]}\033[0m";
echo -e "|Hosts:\t\t\t \033[34m$numberone\033[0m       ";
echo "+---------------------------------------+";