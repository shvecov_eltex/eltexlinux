#!/bin/sh
menu=`zenity --list --title="DmitryIDE" \
--text="Выберите действие" \
--column="" \
"New file" \
"Create file"`;
case $menu in
	"New file")newfilename=`zenity --entry --title="Name of file:" --text="Введите имя файла"`;
	touch $newfilename;
	edit_text=`zenity --text-info --editable --width=550 --height=500`;
  	save_path=`zenity --file-selection --filename="$newfilename" --save --confirm-overwrite`;
  	echo -n "$edit_text" > "$save_path";;
	"Create file")
path=`zenity --file-selection`;
case $? in 
	0)text=`cat "$path"`;
  	edit_text=`echo -n "$text" | zenity --text-info --editable --width=550 --height=500`;
  	save_path=`zenity --file-selection --filename="$path" --save --confirm-overwrite`;
  	echo -n "$edit_text" > "$save_path";;
	1)echo  "Entered Close...";;
   -1)echo  "Error";;
esac
esac